import UIKit
import RxSwift

example(of: "FlatMap 1") {
    class PO {
        var age = Variable<Int>(0)
        
        required init(age: Variable<Int>) {
            self.age = age
        }
    }
  
    let disposeBag = DisposeBag()
    
    let jasmine = PO(age: Variable(18))
    let joshua = PO(age: Variable(80))
    
    let po = PublishSubject<PO>()
    
    po.flatMap({ thePO -> Observable<Int> in
        if thePO.age.value > 65 {
            return Observable.empty()
        }
       
        return thePO.age.asObservable()
    }).subscribe(onNext: {
        print($0)
    }).disposed(by: disposeBag)
    
    po.onNext(jasmine)
    po.onNext(joshua)
    
    jasmine.age.value = 19
    jasmine.age.value = 20
    
//    joshua.age.value = 81
//    joshua.age.value = 30
    
//    jasmine.age.value = 90
    
    
    
    
}



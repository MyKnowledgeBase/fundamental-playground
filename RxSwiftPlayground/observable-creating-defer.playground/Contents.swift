import UIKit
import RxSwift

example(of: "defer") {
    
    // init value, subscription.
    var value: String? = nil
    var value1: String? = nil
//    var subscription: Observable<String?> = Observable.of(value)
    var subscription: Observable<String?> = Observable.deferred {
        //will be executed only after subscription.
        return Observable.of(value, value1)
    }
    
    value = "hello"
    value = "world"
    subscription.subscribe(onNext: { event in
        print(event)
    }, onCompleted: {
        print("completed")

    }, onDisposed: {
        print("disposed")
    })
    value = "Hi there..."
}


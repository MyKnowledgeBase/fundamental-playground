import UIKit
import RxSwift

example(of: "empty") {
    let emptySequence = Observable<Int>.empty()

    let dispose = emptySequence.subscribe { print($0) }

//    let subject = PublishSubject<Int>()
//
//    subject.flatMap({ _ -> Observable<String> in
//        return Observable<String>.just("hello wrold")
//    })
    
    let observable = Observable<Int>.just(1)
    
    observable.flatMap({ _ in
        return Observable<String>.just("hello world")
    })
    
}

